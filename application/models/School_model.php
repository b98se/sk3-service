<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class School_model extends CI_Model {


    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }

    function get_school($smis) {
        $this->db->from('schools')->where('smis', $smis);

        $query = $this->db->get();
        return $query->row();
    }

    public function fetch_schools($amphur = false) {
        $this->db->from('schools');
        if($amphur){
            $this->db->where("amphur", $amphur);
        }
        $this->db->order_by(" CONVERT (name USING tis620) ", "asc");
        $query = $this->db->get();
        return $query->result();
    }

    public function fetch_amphur_school_count() {
        $this->db->select('amphur_id, amphur, count(*) as cnt')
            ->from('schools')
            ->group_by('amphur_id, amphur');
        $query = $this->db->get();
        return $query->result();
    }

}
