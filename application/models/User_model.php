<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {

    public $display_name;
    public $username;

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function chk_username_password($username, $password = '') {
        $password = $password == '' ? $username : $password;
        
        $this->db->from('users')
            ->where('username', $username)
            ->where('password', $password)
            ->limit(1);
        $query = $this->db->get();

        if ($query->num_rows() == 1) {
            $this->User_model->update_lastseen($username);
            return $query->result();
        } else {
            return false;
        }
    }

    public function update_lastseen($username) {
        $data['lastseen'] = date("Ymd-His");
        $this->db->where('username', $username)->update('users', $data);
    }

    function get_user($id) {
        $this->db->from('users')->where('username', $id);

        $query = $this->db->get();
        return $query->row();
    }

    public function fetch_users($sort_id = false) {
        $this->db->from('users');
        if($sort_id){
            $this->db->order_by("sort_id", "asc");
        }else{
            $this->db->order_by("display_name", "asc");
        }
        $query = $this->db->get();
        return $query->result();
    }

    public function get_school($id) {
        $this->db->select('username, display_name')
                ->from('users')
                ->where('username', $id)
                ->order_by("display_name", "asc");
        $query = $this->db->get();
        return $query->result();
    }

}
