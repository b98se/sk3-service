<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class List1_model extends CI_Model {


    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }

    function get_list1($id) {
        $this->db->from('list1s')->where('id', $id);

        $query = $this->db->get();
        return $query->row();
    }

    public function fetch_list1s($list1 = false) {
        $this->db->from('list1s');
        if($list1){
            $this->db->where("list1", $list1);
        }
        $this->db->order_by("sort1", "asc");
        $query = $this->db->get();
        return $query->result();
    }
    public function fetch_ar_list1s($list1 = false) {
        $this->db->from('list1s');
        if($list1){
            $this->db->where("list1", $list1);
        }
        $this->db->order_by("sort1", "asc");
        $query = $this->db->get();
        foreach($query->result() as $o){
            $ar[$o->key1] = $o->value1;
        }
        return $ar;
    }

}
