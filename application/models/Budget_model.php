<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Budget_model extends CI_Model {


    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }

    function get_budget($id) {
        $this->db->from('budgets')->where('budgets.id', $id);

        $query = $this->db->get();
        return $query->row();
    }

    public function fetch_budgets() {
        $this->db->from('budgets');
        $query = $this->db->get();
        return $query->result();
    }
    
    public function fetch_school_budgets($smis_id = false) {
        $this->db->select('budgets.*, smis_id, budget, etc')
            ->from('budgets')
            ->join('budget_details', 'budgets.id = budget_details.budget_id');
        if($smis_id){
            $this->db->where("smis_id", $smis_id);
        }

        $query = $this->db->get();
        return $query->result();
    }

    function get_budget_detail($id) {
        $this->db->from('budget_details')->where('id', $id);

        $query = $this->db->get();
        return $query->row();
    }

    public function fetch_budget_details($budget_id = false) {
        $this->db->select('budget_details.*, schools.name ,schools.amphur')
          ->from('budget_details')
          ->join('schools', 'schools.smis = budget_details.smis_id');
        if($budget_id){
            $this->db->where("budget_id", $budget_id);
        }
        $query = $this->db->get();
        return $query->result();
    }

    public function get_sum_budget_details ($budget_id = false) {
        $this->db->select_sum('budget')
            ->from('budget_details')
            ->where('budget_id', $budget_id);

        $query = $this->db->get();
        return $query->row();
    }
    
}
