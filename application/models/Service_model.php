<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Service_model extends CI_Model {


    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }

    function get_service($id) {
        $this->db->select('services.*, schools.name ,schools.amphur, users.display_name')
            ->from('services')
            ->join('schools', 'schools.smis = services.smis')
            ->join('users', 'users.id = services.user_id', 'left')
            ->where('services.id', $id);

        $query = $this->db->get();
        return $query->row();
    }

    public function fetch_0_services($smis = false, $year1 = false) {
        $this->db->select('services.*, users.display_name, schools.name ,schools.amphur')
            ->from('services')
            ->join('users', 'users.id = services.user_id')
            ->join('schools', 'schools.smis = services.smis');
        if($smis){
            $this->db->where("services.smis", $smis);
        }
        if($year1){
            $this->db->where('s_date >=', ($year1 - 1).'-10-01'); //first_date
            $this->db->where('s_date <=', $year1.'-09-30'); //second_date
        }
        $this->db->order_by("id", "desc");
        return $this->db->get();
        //$query = $this->db->get();
        //return $query->result();
    }
    public function fetch_0_services_service_id($smis = false, $year1 = false) {
        $this->db->select('services.*, users.display_name, schools.name ,schools.amphur, service_id')
            ->from('services')
            ->join('users', 'users.id = services.user_id')
            ->join('schools', 'schools.smis = services.smis')
            ->join('service_details', 'services.id = service_details.service_id', 'left');
        if($smis){
            $this->db->where("services.smis", $smis);
        }
        if($year1){
            $this->db->where('s_date >=', ($year1 - 1).'-10-01'); //first_date
            $this->db->where('s_date <=', $year1.'-09-30'); //second_date
        }
        $this->db->order_by("id", "desc");
        return $this->db->get();
        //$query = $this->db->get();
        //return $query->result();
    }
    
    public function fetch_services($smis = false) {
        $query = $this->fetch_0_services($smis);
        return $query->result();
    }
    
    public function get_last_services($smis = false, $year1 = false) {
        $query = $this->fetch_0_services_service_id($smis, $year1);
        return $query->row();
    }

    function get_service_detail($id) {
        $this->db->from('service_details')->where('id', $id);
        
        $query = $this->db->get();
        return $query->row();
    }
    
    public function fetch_service_details($service_id = false) {
        $this->db->from('service_details');
        if($service_id){
            $this->db->where("service_id", $service_id);
        }
        $query = $this->db->get();
        return $query->result();
    }
    
    public function fetch_amphur_service_count($year1 = false) {
        $this->db->select('amphur_id, amphur, count(DISTINCT('.$this->db->dbprefix('services').'.smis)) as cnt')
            ->from('services')
            ->join('schools', 'schools.smis = services.smis')
            ->group_by('amphur_id, amphur');
        if($year1){
            $this->db->where('s_date >=', ($year1 - 1).'-10-01'); //first_date
            $this->db->where('s_date <=', $year1.'-09-30'); //second_date
        }
        $query = $this->db->get();
        return $query->result();
    }
}
