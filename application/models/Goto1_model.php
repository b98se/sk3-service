<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Goto1_model extends CI_Model {


  public function __construct() {
      // Call the CI_Model constructor
      parent::__construct();
  }

  function get_goto1($id) {
      $this->db->from('gotos')->where('id', $id);

      $query = $this->db->get();
      return $query->row();
  }

  public function fetch_goto1s($year1 = false) {
      $this->db->select(" gotos.*, schools.name ,schools.amphur")
        ->from('gotos')
        ->join('schools', 'schools.smis = gotos.smis');
      if($year1){
        $this->db->where("year1", $year1);
      }
      //$this->db->order_by("year1", "asc");
      $query = $this->db->get();
      return $query->result();
  }

  public function fetch_goto1_years() {
      $this->db->select('year1, count(id) as cnt')
        ->from('gotos')
        ->group_by("year1")
        ->order_by("year1", "asc");
      $query = $this->db->get();
      return $query->result();
  }

  public function fetch_service_goto1s($year1 = false) {
      $this->db->select(" gotos.*, schools.name ,schools.amphur, services.id as s_id, stype_id, s_date")
        ->from('gotos')
        ->join('schools', 'schools.smis = gotos.smis')
        ->join('services', 'services.smis = gotos.smis', 'left');
      if($year1){
        $this->db->where("year1", $year1);
      }
      //$this->db->order_by("year1", "asc");
      $query = $this->db->get();
      return $query->result();
  }

  public function fetch_amphur_goto_count($year1 = false) {
    $this->db->select('amphur_id, amphur, count(*) as cnt')
        ->from('gotos')
        ->join('schools', 'schools.smis = gotos.smis')
        ->group_by('amphur_id, amphur');
    if($year1){
        $this->db->where('date1 >=', ($year1 - 1).'-10-01'); //first_date
        $this->db->where('date1 <=', $year1.'-09-30'); //second_date
    }
    $query = $this->db->get();
    return $query->result();
  }

}
