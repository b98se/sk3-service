<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Service_detail extends CI_Controller {
  function __construct() {
    parent::__construct();
    $this->load->model('School_model', '', TRUE);
    $this->load->model('List1_model', '', TRUE);
    $this->load->model('Service_model', '', TRUE);

    if($this->session->userdata('logged_in')){
      $this->logged_in = $this->session->userdata('logged_in');
    }else{
      $this->logged_in = false;
    }
  }
  
	public function index()
	{
    if(!$this->logged_in){
      
      $this->session->set_flashdata('alert','danger | กรุณา Login เข้าสู่ระบบ !!! ');
      redirect(site_url('/'));
    }
    
    $d['title'] = 'รายชื่อซ่อมบำรุง';
    $d['services'] = $this->Service_model->fetch_services();
    // $d['sch_sizes'] = $this->List1_model->fetch_ar_list1s('schsize');
    
		$this->template->set('title', $d['title']);
		$this->template->load('template', 'services_view', $d);
  }
  
  public function info($id)
  {
    $d['title'] = 'ข้อมูลซ่อมบำรุง';
    $d['service'] = $this->Service_model->get_service($id);
    $d['svds'] = $this->Service_model->fetch_service_details($d['service']->id);
    $d['stypes'] = $this->List1_model->fetch_ar_list1s('stype');
    
    $this->template->set('title', $d['title']);
    $this->template->load('template', 'service_view', $d);
  }
  
  public function add($service_id, $stype_id)
  {
    $d['service_id'] = $service_id;
    $d['items'] = $this->List1_model->fetch_ar_list1s('item-'.$stype_id);
    $d['title'] = "เพิ่มข้อมูลการซ่อมบำรุง";

    $this->template->set('title', $d['title']);
    $this->template->load('template', 'service_detail_add_view', $d);
    
  }
  public function insert()
  {
    $p = $this->input->post();

    $p["insert_at"] = date("Y-m-d H:i:s");
    unset($p["pic"]);

    $this->db->insert('service_details', $p);
    $insert_id = $this->db->insert_id();

    if($this->db->affected_rows() > 0){

      //  =====  upload file =======
      $config['upload_path'] = $this->config->item('upload_path');
      $config['overwrite'] = TRUE;
      $config['allowed_types'] = 'jpg';
      $config['file_name'] = 'service_detail_'.$insert_id;

      $this->load->library('upload', $config);

      if (!$this->upload->do_upload('pic')){
        $upload = $this->upload->display_errors();
      }else{
        $upload = $this->upload->data();
      }
      //  =====  upload file =======

      $this->session->set_flashdata('alert','success | บันทึกข้อมูลเรียบร้อยแล้ว !!! '.$upload);
      redirect(site_url('service/info/') . $p['service_id']);
    }else{
      $this->session->set_flashdata('alert','danger | เกิดข้อผิดพลาดในการบันทึกข้อมูล !!! '.$upload);
      redirect($_SERVER['HTTP_REFERER']);
    }

  }

  public function edit($service_detail_id)
  {
    $d['id'] = $service_detail_id;
    
    $d['service_detail'] = $this->Service_model->get_service_detail($service_detail_id);
    $d['service'] = $this->Service_model->get_service($d['service_detail']->service_id);

    $d['items'] = $this->List1_model->fetch_ar_list1s('item-'.$d['service']->stype_id);
    $d['title'] = "แก้ไขข้อมูลการซ่อมบำรุง";

    $this->template->set('title', $d['title']);
    $this->template->load('template', 'service_detail_edit_view', $d);
    
  }
  public function update()
  {
    $p = $this->input->post();
    $id = $p['id'];
    $p["update_at"] = date("Y-m-d H:i:s");

    //  =====  upload file =======
    $config['upload_path'] = $this->config->item('upload_path');
    $config['overwrite'] = TRUE;
    $config['allowed_types'] = 'jpg';
    $config['file_name'] = 'service_detail_'.$id;

    $this->load->library('upload', $config);

    if (!$this->upload->do_upload('pic')){
      $upload = $this->upload->display_errors();
    }else{
      $upload = $this->upload->data();
    }
    //  =====  upload file =======

    unset($p["pic"], $p['id'], $p['insert_at']);

    $this->db->where('id', $id);
    $this->db->update('service_details', $p);

    if($this->db->affected_rows() > 0){
      $service_detail = $this->Service_model->get_service_detail($id);

      $this->session->set_flashdata('alert','success | บันทึกข้อมูลเรียบร้อยแล้ว !!! '.$upload);
      redirect(site_url('service/info/') . $service_detail->service_id);
    }else{
      $this->session->set_flashdata('alert','danger | เกิดข้อผิดพลาดในการบันทึกข้อมูล !!! '.$upload);
      redirect($_SERVER['HTTP_REFERER']);
    }

  }

}
