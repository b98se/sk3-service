<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Service extends CI_Controller {
  function __construct() {
    parent::__construct();
    $this->load->model('School_model', '', TRUE);
    $this->load->model('List1_model', '', TRUE);
    $this->load->model('Service_model', '', TRUE);
    $this->logged_in = $this->session->userdata('logged_in');

  }
  
	public function index()
	{
    
    $d['title'] = 'รายชื่อซ่อมบำรุง';
    $d['services'] = $this->Service_model->fetch_services();
    $d['stypes'] = $this->List1_model->fetch_ar_list1s('stype');
    
		$this->template->set('title', $d['title']);
		$this->template->load('template', 'services_view', $d);
  }
  
  public function info($id, $print = '')
  {
    $d['title'] = 'ข้อมูลซ่อมบำรุง';
    $d['service'] = $this->Service_model->get_service($id);
    $d['school'] = $this->School_model->get_school($d['service']->smis);
    $d['svds'] = $this->Service_model->fetch_service_details($d['service']->id);
    $d['stypes'] = $this->List1_model->fetch_ar_list1s('stype');
    $d['items'] = $this->List1_model->fetch_ar_list1s('item-'.$d['service']->stype_id);
    
    $this->template->set('title', $d['title']);
    if($print == 'print'){
      $this->template->load('template_'.$print, 'service_'.$print.'_view', $d);
    }else{
      $this->template->load('template', 'service_view', $d);
    }
  }
  
  public function add($smis = false)
  {
    auth1();
    $d['schools'] = $this->School_model->fetch_schools();
    $d['stypes'] = $this->List1_model->fetch_ar_list1s('stype');

    
    $d['smis'] = $smis;
    $d['title'] = "เพิ่มข้อมูลการซ่อมบำรุง";

    $this->template->set('title', $d['title']);
    $this->template->load('template', 'service_add_view', $d);
    
  }
  public function insert()
  {
    auth1();
    $p = $this->input->post();

    $p['user_id'] = $this->logged_in['id'];
    $p['s_date'] = date_thai2my($p['s_date']);
    $p["insert_at"] = date("Y-m-d H:i:s");
    $stype_id = $p["stype_id"];

    $this->db->insert('services', $p);
    $insert_id = $this->db->insert_id();

    if($this->db->affected_rows() > 0){
      $this->session->set_flashdata('alert','success | บันทึกข้อมูลเรียบร้อยแล้ว !!! ');
      redirect(site_url('service_detail/add/') . $insert_id .'/'. $stype_id);
      // /service_detail/add/115/dlit
    }else{
      $this->session->set_flashdata('alert','danger | เกิดข้อผิดพลาดในการบันทึกข้อมูล !!! ');
      redirect($_SERVER['HTTP_REFERER']);
    }

  }

  public function edit($id)
  {
    auth1();
    $d['service'] = $this->Service_model->get_service($id);
    $d['schools'] = $this->School_model->fetch_schools();
    $d['stypes'] = $this->List1_model->fetch_ar_list1s('stype');

    
    $d['title'] = "แก้ไขข้อมูลการซ่อมบำรุง";

    $this->template->set('title', $d['title']);
    $this->template->load('template', 'service_edit_view', $d);
    
  }
  public function update()
  {
    auth1();
    $p = $this->input->post();
    $id = $p['id'];
    $p['s_date'] = date_thai2my($p['s_date']);
    $p["update_at"] = date("Y-m-d H:i:s");
    
    unset($p['id'],$p['insert_at']);
    
    $this->db->where('id', $id);
    $this->db->update('services', $p);
    
    if($this->db->affected_rows() > 0){
      $this->session->set_flashdata('alert','success | บันทึกข้อมูลเรียบร้อยแล้ว !!! ');
      redirect(site_url('service/info/') . $id);
    }else{
      $this->session->set_flashdata('alert','danger | เกิดข้อผิดพลาดในการบันทึกข้อมูล !!! ');
      redirect($_SERVER['HTTP_REFERER']);
    }
    
  }
  
  public function delete($id)
  {
    auth1();
    $this->db->delete('service_details', array('service_id' => $id));  // Produces: // DELETE FROM mytable  // WHERE id = $id
    $this->db->delete('services', array('id' => $id));  // Produces: // DELETE FROM mytable  // WHERE id = $id

    redirect(site_url('service/'));
    
  }
  public function detail_delete($service_id, $id)
  {
    auth1();
    $this->db->delete('service_details', array('id' => $id));  // Produces: // DELETE FROM mytable  // WHERE id = $id
    
    $path = $this->config->item('upload_path').'service_detail_'.$id.'.jpg';
    $result = unlink($path); 
    
    redirect(site_url('service/info/'.$service_id));
    
  }
  
  public function report_amphur_count ($year = false){

    $year = $year ? $year : year_season();

    $d['tbody'] = $this->Service_model->fetch_amphur_service_count($year);

    $d['thead'] = ['อำเภอ', 'จำนวน'];
    $d['columns'] = ['amphur', 'cnt'];

    $d['title'] = "รายงานการจำนวนการซ่อมบำรุงแยกตามอำเภอ";

    $this->template->set('title', $d['title']);
    $this->template->load('template', 'report_datatable_view', $d);
  }

}
