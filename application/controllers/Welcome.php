<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct() {
    parent::__construct();
    $this->load->model('School_model', '', TRUE);
    $this->load->model('Service_model', '', TRUE);
    $this->load->model('Goto1_model', '', TRUE);

  }

	public function index()
	{
		// $this->load->view('welcome_message');
		if($this->session->userdata('logged_in')){
			$this->template->set('title', 'หน้าแรก');
			$this->template->load('template', 'blank_view');
		}else{
			$this->template->set('title', 'หน้าแรก');
			$this->template->load('template', 'login_view');
		}
	}

	public function report1 ($year = false)
	{
		//fetch_amphur_goto_count fetch_amphur_service_count
		$year = $year ? $year : year_season();

    $school_count = $this->School_model->fetch_amphur_school_count();
    $service_count = $this->Service_model->fetch_amphur_service_count($year);
		$goto_count = $this->Goto1_model->fetch_amphur_goto_count($year);

		$d1 = [];
		foreach($school_count as $sc){
			$obj = new stdClass();
			$obj->amphur_id = $sc->amphur_id;	
			$obj->amphur = $sc->amphur;	
			$obj->cnt = $sc->cnt;

			// findObjectById($id, $array, $find, $return)
			$obj->gt_cnt = findObjectById('amphur_id', $goto_count, $sc->amphur_id, 'cnt');
			$obj->sv_cnt = findObjectById('amphur_id', $service_count, $sc->amphur_id, 'cnt');

			array_push($d1,$obj);
		}


    $d['thead'] = ['อำเภอ', 'จำนวนโรงเรียน', 'เป้าหมาย', 'ลงพื้นที่แล้ว'];
    $d['columns'] = ['amphur', 'cnt', 'gt_cnt', 'sv_cnt'];
		$d['tbody'] = $d1;

    $d['title'] = "รายงานสรุปตามอำเภอ";

		$this->template->set('title', 'หน้าแรก');
		$this->template->load('template', 'report_datatable_view', $d);

	}

}


