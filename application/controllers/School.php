<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class School extends CI_Controller {
  function __construct() {
    parent::__construct();
    $this->load->model('School_model', '', TRUE);
    $this->load->model('List1_model', '', TRUE);
    $this->load->model('Service_model', '', TRUE);
    $this->load->model('Budget_model', '', TRUE);

  }
  
	public function index()
	{
    $d['title'] = 'รายชื่อโรงเรียน';
    $d['schools'] = $this->School_model->fetch_schools();
    $d['sch_sizes'] = $this->List1_model->fetch_ar_list1s('schsize');
    
		$this->template->set('title', $d['title']);
		$this->template->load('template', 'schools_view', $d);
  }
  
  public function info($smis)
  {
    $d['title'] = 'ข้อมูลโรงเรียน';
    $d['school'] = $this->School_model->get_school($smis);
    $d['services'] = $this->Service_model->fetch_services($smis);
    $d['school_budgets'] = $this->Budget_model->fetch_school_budgets($smis);
    $d['stypes'] = $this->List1_model->fetch_ar_list1s('stype');
    
    $this->template->set('title', $d['title']);
    $this->template->load('template', 'school_view', $d);
  }

  public function edit($smis)
  {
    auth1();
    $d['school'] = $this->School_model->get_school($smis);

    
    $d['title'] = "แก้ไขข้อมูลโรงเรียน";

    $this->template->set('title', $d['title']);
    $this->template->load('template', 'school_edit_view', $d);
    
  }
  public function update()
  {
    auth1();
    $p = $this->input->post();
    $p['update_at'] = date("Y-m-d H:i:s");
    $smis = $p['smis'];

    unset($p['smis']);

    $this->db->where('smis', $smis);
    $result = $this->db->update('schools', $p);

    db_result_redirect($result, site_url('school/info/'.$smis) );
  }

  public function report_amphur_count (){

    $d['tbody'] = $this->School_model->fetch_amphur_school_count();

    $d['thead'] = ['อำเภอ', 'จำนวน'];
    $d['columns'] = ['amphur', 'cnt'];

    $d['title'] = "รายงานการจำนวนการซ่อมบำรุงแยกตามอำเภอ";

    $this->template->set('title', $d['title']);
    $this->template->load('template', 'report_datatable_view', $d);
  }

}
