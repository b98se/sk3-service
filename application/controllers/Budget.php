<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Budget extends CI_Controller {
  function __construct() {
    parent::__construct();
    $this->load->model('School_model', '', TRUE);
    $this->load->model('List1_model', '', TRUE);
    $this->load->model('Budget_model', '', TRUE);

  }
  
	public function index()
	{
    
    $d['title'] = 'รายการจัดสรรงบประมาณ';
    $d['budgets'] = $this->Budget_model->fetch_budgets();
    $d['stypes'] = []; //$this->List1_model->fetch_ar_list1s('stype');
    
		$this->template->set('title', $d['title']);
		$this->template->load('template', 'budgets_view', $d);
  }
  
  public function info($id, $print = '')
  {
    $d['title'] = 'ข้อมูลการจัดสรรงบประมาณ';
    $d['budget'] = $this->Budget_model->get_budget($id);

    $d['budget_details'] = $this->Budget_model->fetch_budget_details($id);
    $d['sum_budget_detail'] = $this->Budget_model->get_sum_budget_details($id);

    $this->template->set('title', $d['title']);
    if($print == 'print'){
      $this->template->load('template_'.$print, 'budget_'.$print.'_view', $d);
    }else{
      $this->template->load('template', 'budget_view', $d);
    }
  }
  
  public function add($smis = false)
  {
    auth1_lv ('area');

    $d['title'] = "เพิ่มข้อมูลการจัดสรรงบประมาณ";

    $this->template->set('title', $d['title']);
    $this->template->load('template', 'budget_add_view', $d);
    
  }
  public function insert()
  {
    auth1_lv ('area');

    $p = $this->input->post();

    //$p['user_id'] = $this->logged_in['id'];
    $p['doc_date'] = date_thai2my($p['doc_date']);
    $p["insert_at"] = date("Y-m-d H:i:s");

    $result = $this->db->insert('budgets', $p);
    $insert_id = $this->db->insert_id();

    db_result_redirect($result, site_url('budget/info/') . $insert_id);

  }

  public function edit($id)
  {
    auth1_lv ('area');

    $d['budget'] = $this->Budget_model->get_budget($id);
    $d['title'] = "แก้ไขข้อมูลการจัดสรรงบประมาณ";

    $this->template->set('title', $d['title']);
    $this->template->load('template', 'budget_edit_view', $d);
    
  }
  public function update()
  {
    auth1_lv ('area');

    $p = $this->input->post();
    $id = $p['id'];
    $p['doc_date'] = date_thai2my($p['doc_date']);
    $p["update_at"] = date("Y-m-d H:i:s");

    unset($p['id'],$p['insert_at']);

    $this->db->where('id', $id);
    $result = $this->db->update('budgets', $p);

    db_result_redirect($result, site_url('budget/info/') . $id);

  }
  
  public function delete($id)
  {
    auth1_lv ('area');

    $result = $this->db->delete('budget_details', array('budget_id' => $id));  
    $result = $this->db->delete('budgets', array('id' => $id));
    db_result_redirect($result, site_url('budget/'));

  }

}
