<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Budget_detail extends CI_Controller {
  function __construct() {
    parent::__construct();
    $this->load->model('School_model', '', TRUE);
    $this->load->model('List1_model', '', TRUE);
    $this->load->model('Budget_model', '', TRUE);

  }
  
	public function index()
	{
  }
  
  
  public function add($budget_id = false)
  {
    auth1_lv ('area');

    $d['budget_id'] = $budget_id;
    $d['title'] = "เพิ่มรายการจัดสรรงบประมาณ";
    $d['schools'] = $this->School_model->fetch_schools();

    $this->template->set('title', $d['title']);
    $this->template->load('template', 'budget_detail_add_view', $d);
    
  }

  public function insert()
  {
    auth1_lv ('area');

    $p = $this->input->post();

    $result = $this->db->insert('budget_details', $p);
    db_result_redirect($result, site_url('budget/info/') . $p['budget_id']);

  }

  public function edit($id)
  {
    auth1_lv ('area');

    $d['title'] = "แก้ไขรายการจัดสรรงบประมาณ";
    $d['budget_detail'] = $this->Budget_model->get_budget_detail($id);
    $d['schools'] = $this->School_model->fetch_schools();

    $this->template->set('title', $d['title']);
    $this->template->load('template', 'budget_detail_edit_view', $d);
    
  }
  public function update()
  {
    auth1_lv ('area');

    $p = $this->input->post();
    $id = $p['id'];

    unset($p['id']);

    $this->db->where('id', $id);
    $result = $this->db->update('budget_details', $p);

    db_result_redirect($result, site_url('budget/info/') . $p['budget_id']);    
  }
  
  public function delete($budget_id, $id)
  {
    auth1_lv ('area');

    $result = $this->db->delete('budget_details', array('id' => $id));  
    db_result_redirect($result, site_url('budget/info/'.$budget_id));

  }

}
