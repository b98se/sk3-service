<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class List1 extends CI_Controller {
  function __construct() {
    parent::__construct();
    $this->load->model('List1_model', '', TRUE);

    if($this->session->userdata('logged_in')){
      $this->logged_in = $this->session->userdata('logged_in');
    }else{
      $this->logged_in = false;
    }
  }
  
  
	public function index()
	{
    if(!$this->logged_in){
      
      $this->session->set_flashdata('alert','danger | กรุณา Login เข้าสู่ระบบ !!! ');
      redirect(site_url('/'));
    }

    $d['title'] = 'รายชื่อรายการในระบบ';
    $d['list1s'] = $this->List1_model->fetch_list1s();

		$this->template->set('title', $d['title']);
		$this->template->load('template', 'list1s_view', $d);
  }
  

}
