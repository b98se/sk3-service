<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Goto1 extends CI_Controller {
  function __construct() {
    parent::__construct();
    $this->load->model('School_model', '', TRUE);
    $this->load->model('List1_model', '', TRUE);
    $this->load->model('Goto1_model', '', TRUE);
    $this->load->model('Service_model', '', TRUE);
    $this->logged_in = $this->session->userdata('logged_in');

  }
  
	public function index()
	{
    
    $d['title'] = 'กำหนดการลงพื้นที่';
    $d['goto1_years'] = $this->Goto1_model->fetch_goto1_years();
  
		$this->template->set('title', $d['title']);
		$this->template->load('template', 'goto1_years_view', $d);
  }
  
	public function index1($year = false)
	{
    $year = $year ? $year : year_season();

    $d['title'] = 'กำหนดการลงพื้นที่';
    $d['goto1s'] = $this->Goto1_model->fetch_goto1s($year);

    foreach($d['goto1s'] as $gt){
      $sv_gt = $this->Service_model->get_last_services($gt->smis, $year);
      $d['goto_service'][$gt->smis] =  $sv_gt;
    }
    
		$this->template->set('title', $d['title']);
		$this->template->load('template', 'goto1s_view', $d);
  }
  
  public function report_amphur_count ($year = false){

    $year = $year ? $year : year_season();

    $d['tbody'] = $this->Goto1_model->fetch_amphur_goto_count($year);

    $d['thead'] = ['อำเภอ', 'จำนวน'];
    $d['columns'] = ['amphur', 'cnt'];

    $d['title'] = "รายงานการจำนวนกำหนดการลงพื้นที่แยกตามอำเภอ";

    $this->template->set('title', $d['title']);
    $this->template->load('template', 'report_datatable_view', $d);
  }
  
}
