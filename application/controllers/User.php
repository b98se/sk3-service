<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {
  function __construct() {
    parent::__construct();
    $this->load->model('User_model', '', TRUE);

  }
    
	public function index()
	{
    auth1_lv('area');

    $d['title'] = 'รายชื่อผู้ใช้';
    $d['users'] = $this->User_model->fetch_users();

		$this->template->set('title', $d['title']);
		$this->template->load('template', 'users_view', $d);
  }
  
	public function login()
  {
    $username = $this->input->post('usr');
    $password = $this->input->post('pwd');
    
    //query the database
    $result = $this->User_model->chk_username_password($username, $password);
    
    if ($result) {
      foreach ($result as $row) {
        $sess_array = array(
          'id' => $row->id,
          'username' => $row->username,
          'display_name' => $row->display_name,
          'user_type' => $row->user_type,
          'amp_code' => $row->amp_code,
        );
        $this->session->set_userdata('logged_in', $sess_array);
      }
      // echo "yes";
      $this->session->set_flashdata('alert','success | Log in success ');
      redirect(site_url('/school'));
    } else {
      // echo "no";
      $this->session->set_flashdata('alert','danger | username และ password ไม่ถูกต้อง !!! ');
      redirect(site_url('/'));
    }
    
  }
  public function logout()
  {
    $this->session->unset_userdata('logged_in');
    session_destroy();
    redirect(site_url('/'), 'refresh');

  }
}
