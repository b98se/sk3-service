<link rel="stylesheet" href="<?php echo base_url("assets"); ?>/css/jquery.dataTables.min.css">

<div class="">

  <div class="columns">
    <div class="column">
      <h5 class="title is-5">ข้อมูลอุปกรณ์ที่ซ่อมบำรุง</h5>  
    </div>
    <?php if(auth1_chk()): ?>
    <div class="column has-text-right">
      <a class="button is-primary" href="<?= site_url('service_detail/add/').$service->id.'/'.$service->stype_id ?>">เพิ่มข้อมูล</a>  
    </div>
    <?php endif; ?>
  </div>


<div class="table-container">
  <table id='table1' class="table is-fullwidth">
    <!-- Your table content -->
    <thead>
      <tr>
        <th>ที่</th>
        <th>ภาพ</th>
        <th>อุปกรณ์</th>
        <th>อาการ</th>
        <th>ยี่ห้อ</th>
        <th>รุ่น</th>
        <th>แนวทางแก้ไข</th>
      </tr>
    </thead>
    <tbody>
      <?php 
      // service_id	item_id	symptom	item_brand	item_model	solve	update_at	insert_at
      $url = site_url('service_detail/edit');
      $trtd = "<tr><td>%s</td> <td>%s</td> <td><a href='$url/%s'>%s</a></td><td>%s</td><td>%s</td><td>%s</td><td>%s</td></tr>";
      
      foreach($svds as $i => $svd){
        $img_link = base_url("uploads").'/service_detail_'.$svd->id.'.jpg';
        $img = ' <figure class="image is-64x64"><a href="'.$img_link.'" target=blank><img src="'.$img_link.'" /> </a></figure>';
        
        $item2 = svd_item_name($items, $svd->item_id, $svd->item_etc);
        echo sprintf($trtd, ($i+1), $img, $svd->id, $item2 ,$svd->symptom,$svd->item_brand,$svd->item_model, $svd->solve);
      }
      ?>
    </tbody>
  </table>
</div>
</div>

<script>

$(document).ready(function() {
    $('#table1').DataTable({"language": {"url": "<?= base_url("assets") ?>/js/datatableThai.json"}});
} );
</script>