<div class="columns is-centered">
  <div class="column is-5-tablet is-4-desktop is-3-widescreen">
    <form action="<?php echo site_url('user/login') ?>" method='post' class="box">
      <div class="field">
        <label for="" class="label">Username</label>
        <div class="control ">
          <input type="text" class="input" name='usr' placeholder="เบอร์โทร 10 หลัก" required>
        </div>
      </div>
      <div class="field">
        <label for="" class="label">Password</label>
        <div class="control ">
          <input type="password" name='pwd' placeholder="*******" class="input" >
        </div>
      </div>
      <div class="field">
        <button class="button is-success">
          Login
        </button>
      </div>
    </form>
  </div>
</div>