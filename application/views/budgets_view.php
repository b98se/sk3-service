<div class="">
  <div class="columns">
    <div class="column">
      <h5 class="title is-5"><?= $title ?></h5>  
    </div>

    <?php if(auth1_lv_chk('area')): ?>
    <div class="column has-text-right">
      <a class="button is-primary" href="<?= site_url('budget/add/') ?>">เพิ่มข้อมูล</a>  
    </div>
    <?php endif; ?>
  </div>

  <div class="table-container">
    <table id='table1' class="table is-fullwidth">
      <!-- Your table content -->
      <thead>
        <tr>
          <th>ที่</th>
          <th>เลขที่</th>
          <th>ลงวันที่</th>
          <th>เรื่อง</th>
        </tr>
      </thead>
      <tbody>
        <?php 
        $url = site_url('budget/info');
        $trtd = "<tr><td>%s</td><td><a href='$url/%s'>%s</a></td><td>%s</td><td>%s</td>
                  </tr>";
        foreach($budgets as $i => $budget){
          echo sprintf($trtd, ($i+1), $budget->id, $budget->doc_no, date_my2thai($budget->doc_date, 'd mmm yyyy'), $budget->name);
        }
        ?>
      </tbody>
    </table>
  </div>
</div>
