<div class="columns is-centered">
  <div class="column is-10-tablet">
    
  <div class="box">
    <h5 class="title is-5"><?= $title ?></h5>
    
    <div class="table-container">
      <table id='table1school' class="table is-fullwidth is-striped table-detail">
        <!-- Your table content -->
        <tr>
          <td>รหัส smis</td>
          <td><?= $school->smis ?></td>
        </tr>
        <tr>
          <td>ชื่อโรงเรียน</td>
          <td><?= $school->name ?></td>
        </tr>
        <tr>
          <td>ตำบล</td>
          <td><?= $school->tambon ?></td>
        </tr>
        <tr>
          <td>อำเภอ</td>
          <td><?= $school->amphur ?></td>
        </tr>
        <tr>
          <td>ผู้บริหาร</td>
          <td><?= $school->ceo ?> <?= echo1($school->ceo_tel, ' (%s) ') ?></td>
        </tr>
        <tr>
          <td>ผู้รับผิดชอบ ICT</td>
          <td><?= $school->ict ?> <?= echo1($school->ict_tel, ' (%s) ') ?></td>
        </tr>
      </table>
    </div>

    <?php if(auth1_chk()): ?>
      <div class='columns'>
        <div class='column '>
          <a class="button is-success" href="<?= site_url('school/edit/').$school->smis ?>">แก้ไข</a>
        </div>
        <div class='column has-text-right is-size-7'>
          ข้อมูล ณ วันที่ <?= datetime_my2thai($school->update_at) ?>
        </div>
      </div>
    <?php endif; ?>


  </div>
  </div>
</div>
<hr>
<?php include("school_budgets_view.php"); ?>
<hr>
<?php include("school_services_view.php"); ?>

<script>

$(document).ready(function() {
    $('#table1school_budgets').DataTable({"language": {"url": "<?= base_url("assets") ?>/js/datatableThai.json"}});
    $('#table1services').DataTable({"language": {"url": "<?= base_url("assets") ?>/js/datatableThai.json"}});
} );
</script>