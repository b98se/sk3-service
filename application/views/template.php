<?php 
$u = empty($this->session->userdata['logged_in']) ? false : $this->session->userdata['logged_in'];
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title> 
      <?= $title ?> ::
      <?= $this->config->item('sys_name_short') ?> ::
      <?= $this->config->item('office_name_short') ?>
    </title>
    <link rel="shortcut icon" href="<?= base_url("assets"); ?>/favicon.ico" />
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Kanit&family=Sarabun&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="<?= base_url("assets"); ?>/css/bulma.min.css">
    <link rel="stylesheet" href="<?= base_url("assets"); ?>/css/app.css">
    <link rel="stylesheet" href="<?php echo base_url("assets"); ?>/css/jquery.dataTables.min.css">

    <script type="text/javascript" src="<?= base_url("assets"); ?>/js/jquery-3.6.0.min.js"></script>
    <script type="text/javascript" src="<?= base_url("assets"); ?>/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="<?= base_url("assets"); ?>/js/app.js"></script>

  </head> 
  <body>

  <section class="header1 is-spaced ">
  <div class="container">
    <h1 class="title has-text-white"><?= $this->config->item('sys_name') ?></h1>
    <h3 class="subtitle has-text-white"><?= $this->config->item('office_name') ?></h3>
  </div>
  </section>


  <nav class="navbar is-warning" role="navigation" aria-label="main navigation">
    <div class="container">
      <div class="navbar-brand">
        <!--a class="navbar-item" href="<?= site_url('/') ?>">
          <img src="https://bulma.io/images/bulma-logo.png" width="112" height="28">
        </a -->

        <a role="button" class="navbar-burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
          <span aria-hidden="true"></span>
          <span aria-hidden="true"></span>
          <span aria-hidden="true"></span>
        </a>
      </div>

      <div id="navbarBasicExample" class="navbar-menu">
      
        <div class="navbar-start">
          <a class="navbar-item" href="<?= site_url('/') ?>">หน้าแรก</a>

          <a class="navbar-item" href="<?= site_url('/school') ?>">โรงเรียน</a>
          <a class="navbar-item" href="<?= site_url('/service') ?>">ซ่อมบำรุง</a>
          <a class="navbar-item" href="<?= site_url('/budget') ?>">งบประมาณ</a>
          <a class="navbar-item" href="<?= site_url('/goto1') ?>">กำหนดการ</a>

          <div class="navbar-item has-dropdown is-hoverable">
            <a class="navbar-link"> รายงาน </a>
            
            <div class="navbar-dropdown">
              <a class="navbar-item" href="<?= site_url('/welcome/report1') ?>" class="button">จำนวนสรุป</a>
              <a class="navbar-item" href="<?= site_url('/school/report_amphur_count') ?>" class="button">จำนวนโรงเรียน</a>
              <a class="navbar-item" href="<?= site_url('/goto1/report_amphur_count') ?>" class="button">จำนวนเป้าหมาย</a>
              <a class="navbar-item" href="<?= site_url('/service/report_amphur_count') ?>" class="button">จำนวนลงพื้นที่แล้ว</a>
            </div>
          </div> <!-- class="navbar dropdown" -->

          
          <a class="navbar-item" href="<?= site_url('/user') ?>">ผู้ใช้</a>

          
        </div>

        <div class="navbar-end">
          <div class="navbar-item has-dropdown is-hoverable">
            <a class="navbar-link">
            <?php 
            echo $u==true ? $u['display_name'] : 'Login';
            ?> 
            </a>
            
            <div class="navbar-dropdown">
              <?php 
              $link = ' <a class="navbar-item" href="%s" class="button">%s</a>';
              if($u){
                echo sprintf($link, site_url('user/logout'), 'Logout');
              }else{
                echo sprintf($link, site_url('/'), 'Login');
              }
              ?>
            </div>
          </div> <!-- class="navbar dropdown" -->

        </div>
      </div>
    </div> <!-- class="container" -->
  </nav>

  <section class="section is-fullheight">
    <div class="container">
      <?php 
      if($this->session->flashdata('alert')): 
        list($alert_type,$alert_txt) = explode('|', $this->session->flashdata('alert'));
        
      ?>
          <div class="columns is-mobile is-centered">
          <div class='column is-6-desktop is-5-widescreen'>

            <div class="notification is-<?= $alert_type ?> is-light has-text-centered">
              <button class="delete"></button>
              <?= $alert_txt ?>
            </div>
          </div>
          </div>
      <?php endif; ?>

      <?= $contents ?>

    </div>
  </section>
  
  <script>
  $(document).ready(function() {
    // Check for click events on the navbar burger icon
    $(".navbar-burger").click(function() {

        // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
        $(".navbar-burger").toggleClass("is-active");
        $(".navbar-menu").toggleClass("is-active");
    });

    $(".notification .delete").click(function() {
      $(".notification").slideUp();
    });

    $('.button-confirm-delete').click(function(event) {
      return confirm('คุณยืนยันจะลบข้อมูลนี้หรือไม่ ?');
    });
  });
  </script>
  </body>
</html>