<div class="field">
  <label for="" class="label">วันที่ซ่อมบำรุง</label>
  <div class="control ">
    <input type="text" class="input" name='s_date'  required>
    <!-- value="<?= date_my2thai(date("Y-m-d")) ?>" -->
  </div>
</div>
<div class="field">
  <label for="" class="label">โรงเรียน</label>
  <div class="control ">
    <div class="select">
      <select name="smis" required>
        <option></option>
        <?php
        $seled = '';
        $sel = "<option value='%s' %s >%s</option>";
        foreach($schools as $sch){
          if(!empty($smis)){
            $seled = $smis == $sch->smis ? 'selected' : '';
          }
          echo sprintf($sel, $sch->smis, $seled, $sch->name);
        }
        ?>
      </select>
    </div>
  </div>
</div>
<div class="field">
  <label for="" class="label">ระบบ</label>
  <div class="control ">
    <div class="select">
      <select name="stype_id" required>
        <option></option>
        <?php
        $sel = "<option value='%s'>%s</option>";
        foreach($stypes as $key => $value){
          echo sprintf($sel, $key, $value);
        }
        ?>
      </select>
    </div>
  </div>
</div>
<div class="field">
  <label for="" class="label">ข้อเสนอแนะเพิ่มเติม</label>
  <div class="control ">
    <textarea class="textarea" name='s_desc'></textarea>
  </div>
</div>
