<!-- 
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `service_id` int(11) DEFAULT NULL,
  `item_id` varchar(8) DEFAULT NULL,
  `item_etc` text,
  `symptom` text,
  `item_brand` varchar(255) DEFAULT NULL,
  `item_model` varchar(255) DEFAULT NULL,
  `solve` text,
  `update_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `insert_at` datetime DEFAULT NULL,
 -->

<div class="field">
  <label for="" class="label">อุปกรณ์</label>
  <div class="control ">
    <div class="select">
      <select name="item_id" id='item_id' required>
        <option></option>
        <?php
        $sel = "<option value='%s' >%s</option>";
        foreach($items as $key => $value){
          echo sprintf($sel, $key, $value);
        }
        echo sprintf($sel, 'etc', 'อื่นๆ');
        ?>
      </select>
    </div>
  </div>
</div>
<div id='div_item_etc' class="field ">
  <label for="item_etc" class="label">อุปกรณ์อื่น (โปรดระบุ)</label>
  <div class="control ">
    <input type="text" class="input" name='item_etc'>
  </div>
</div>
<div class="field">
  <label for="symptom" class="label">อาการ</label>
  <div class="control ">
    <input type="text" class="input" name='symptom'  required>
  </div>
</div>
<div class="field">
  <label for="item_brand" class="label">ยี่ห้อ</label>
  <div class="control ">
    <input type="text" class="input" name='item_brand'  required>
  </div>
</div>
<div class="field">
  <label for="item_model" class="label">รุ่น</label>
  <div class="control ">
    <input type="text" class="input" name='item_model'  required>
  </div>
</div>

<div class="field">
  <label for="solve" class="label">แนวทางแก้ไข</label>
  <div class="control ">
    <textarea class="textarea" name='solve'></textarea>
  </div>
</div>

<div class="field">
  <label for="work" class="label">การดำเนินการของเครือข่ายปฏิบัติงาน</label>
  <div class="control ">
    <textarea class="textarea" name='work'></textarea>
  </div>
</div>

<div class="field">
  <label for="pic" class="label">ภาพอุปกรณ์ (ไฟล์ jpg เท่านั้น)</label>
  <div class="control ">
    <input type="file" id="pic" name="pic" >
  </div>
</div>

<script>
$(document).ready(function(){
  $('#div_item_etc').hide();
});

$('#item_id').on('change', function() {
  // console.log( this.value );
  if(this.value == 'etc'){
    $('#div_item_etc').show();
    // console.log( '#div_item_etc show' );
  }else{
    $('#div_item_etc').hide();
    // console.log( '#div_item_etc hide' );
  }
});
</script>
