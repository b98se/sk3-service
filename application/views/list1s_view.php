<link rel="stylesheet" href="<?php echo base_url("assets"); ?>/css/jquery.dataTables.min.css">

<div class="box">
<h5 class="title is-5"><?= $title ?></h5>

<div class="table-container">
  <table id='table1' class="table is-fullwidth">
    <!-- Your table content -->
    <thead>
      <tr>
        <th>ที่</th>
        <th>ประเภท</th>
        <th>key</th>
        <th>value</th>
        <th>ลำดับ</th>
      </tr>
    </thead>
    <tbody>
      <?php 
      $trtd = '<tr><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td></tr>';
      foreach($list1s as $list){
        echo sprintf($trtd, $list->id,$list->list1,$list->key1,$list->value1, $list->sort1);
      }
      ?>
    </tbody>
  </table>
</div>
</div>

<script>

$(document).ready(function() {
    $('#table1').DataTable({"language": {"url": "<?= base_url("assets") ?>/js/datatableThai.json"}});
} );
</script>