<div class="columns is-centered">
  <div class="column is-8-tablet is-6-desktop">
  <h5 class="title is-5"><?= $title ?></h5>

    <form action="<?php echo site_url('service_detail/insert') ?>" method='post' class="box" enctype="multipart/form-data">
      <input type="hidden" name="service_id" value="<?= $service_id ?>">
      <?php include("service_detail_form_view.php") ?>

      <div class="field">
        <button class="button is-success">
          บันทึกข้อมูล
        </button>
      </div>
    </form>
  </div>
</div>