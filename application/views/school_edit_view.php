<div class="columns is-centered">
  <div class="column is-8-tablet is-6-desktop">
  <h5 class="title is-5"><?= $title ?></h5>

    <form action="<?php echo site_url('school/update') ?>" method='post' class="box">
      <input type="hidden" name="smis" value="<?= $school->smis ?>">
      
      <div id='' class="field ">
        <label for="ceo" class="label">ผู้บริหาร</label>
        <div class="control ">
          <input type="text" class="input" name='ceo'>
        </div>
      </div>
      <div id='' class="field ">
        <label for="ceo_tel" class="label">เบอร์โทรผู้บริหาร</label>
        <div class="control ">
          <input type="text" class="input" name='ceo_tel'>
        </div>
      </div>

      <div id='' class="field ">
        <label for="ict" class="label">ผู้รับผิดชอบ ict</label>
        <div class="control ">
          <input type="text" class="input" name='ict'>
        </div>
      </div>
      <div id='' class="field ">
        <label for="ict_tel" class="label">เบอร์โทร ict</label>
        <div class="control ">
          <input type="text" class="input" name='ict_tel'>
        </div>
      </div>


      <div class='columns'>
        <div class='column has-text-left'>
          <div class="field"><button class="button is-success">บันทึกข้อมูล</button></div>
        </div>
      </div>

    </form>
  </div>
</div>

<script>
jsObject = <?= json_encode($school) ?>

$.each( jsObject, function( key, value ) { 
  //console.log( key + ": " + value );
  $("[name="+key+"]").val(value);
});
</script>