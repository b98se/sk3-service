<div class="container">
  <div class="column has-text-centered ">
    <span class='has-text-weight-bold'>บัญชีจัดสรรงบประมาณ</span>
  </div>
  <div class='columns'>
    <div class="column">
      เลขที่ <?= $budget->doc_no ?>
    </div>
    <div class="column">
      ลงวันที่ <?= date_my2thai($budget->doc_date, 'd mmm yyyy') ?>
    </div>
    <div class="column">
      เรื่อง <?= $budget->name ?>
    </div>
  </div>

  <div class="column ">
    <table id='table1' class="table is-bordered is-fullwidth">
      <!-- Your table content -->
      <thead>
        <tr>
          <th width='10%' >ที่</th>
          <th width='30%'>โรงเรียน</th>
          <th width='15%'>อำเภอ</th>
          <th width='15%'>งบประมาณ</th>
          <th width='30%'>หมายเหตุ</th>
        </tr>
      </thead>
      <tbody>
        <?php 
        // service_id	item_id	symptom	item_brand	item_model	solve	update_at	insert_at
        $trtd = "<tr><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td></tr>";
        foreach($budget_details as $i => $budget_detail){
          echo sprintf($trtd, (++$i), $budget_detail->name, $budget_detail->amphur, $budget_detail->budget, $budget_detail->etc);
        }
        ?>
      </tbody>
    </table>
  </div>


</div>
