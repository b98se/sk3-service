<div class="columns is-mobile is-centered">
  <div class="column is-half">
    <div class="is-primary has-text-centered">
    <h1 class="title"><?= $this->config->item('sys_name_short') ?></h1>
    <h2 class="subtitle"><?= $this->config->item('sys_name') ?></h2>
      
    </div>
  </div>
</div>