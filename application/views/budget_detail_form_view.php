

<div class="field">
  <label for="" class="label">โรงเรียน</label>
  <div class="control ">
    <div class="select">
      <select name="smis_id" id='smis_id' required>
        <option></option>
        <?php
        $sel = "<option value='%s' >%s (%s)</option>";
        foreach($schools as $school){
          echo sprintf($sel, $school->smis, $school->name, $school->amphur);
        }
        ?>
      </select>
    </div>
  </div>
</div>
<div id='div_item_etc' class="field ">
  <label for="budget" class="label">งบประมาณ</label>
  <div class="control ">
    <input type="text" class="input" name='budget'>
  </div>
</div>
<div class="field">
  <label for="etc" class="label">หมายเหตุ</label>
  <div class="control ">
    <input type="text" class="input" name='etc'>
  </div>
</div>

<script>

</script>
