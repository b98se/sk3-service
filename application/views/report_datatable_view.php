<link rel="stylesheet" href="<?php echo base_url("assets"); ?>/css/jquery.dataTables.min.css">
<script type="text/javascript" src="//cdn.datatables.net/plug-ins/1.12.1/api/sum().js"></script>

<div class="">

  <div class="column">
    <h5 class="title is-5"><?= $title ?></h5>  
  </div>

  <div class="table-container">
  <table id='table1' class="table is-fullwidth" style="width:96%">
    <!-- Your table content -->
    <thead>
      <tr>
        <?php
        $col_cnt = $tfoot = '';
        foreach($thead as $th){
          echo "<th class='sum'> $th </th> ";
          $tfoot .= "<th>  </th> ";
          $col_cnt++;
        }
        ?>
      </tr>
    </thead>
    <tfoot>
      <tr>
        <?= $tfoot ?>
      </tr>
    </tfoot>
    
  </table>
  </div>
</div>

<script>

$(document).ready(function() {
    var dataSet = JSON.parse('<?php echo json_encode($tbody); ?>' );
    //console.log(dataSet);

    var table = $('#table1').DataTable(
      {
        "language": {"url": "<?= base_url("assets") ?>/js/datatableThai.json"}, 
        "paging": false,
        "data": dataSet,
        "columns": [ 
          <?php foreach($columns as $c) echo " { data: '$c' }, "; ?>
          ],
        "drawCallback": function () {
		      var api = this.api();
		      $(api.column(0).footer()).html('รวม');
		      for(var i=1; i<= <?= $col_cnt ?>;i++)
		        {$(api.column(i).footer()).html( api.column(i).data().sum() );}
		      
		    }  
      }
    );


} );
</script>