<div class="columns is-centered">
  <div class="column is-10-tablet ">
    
  <div class="box">
    <h5 class="title is-5"><?= $title ?></h5>
    
    <div class="table-container">
      <table id='table1' class="table is-fullwidth is-striped table-detail">
        <!-- Your table content 
        id user_id smis stype_id s_date s_desc update_at insert_at         -->
        <tr>
          <td>ชื่อ</td>
          <td><?= $budget->name ?></td>
        </tr>
        <tr>
          <td>เลขที่</td>
          <td><?= $budget->doc_no ?></td>
        </tr>
        <tr>
          <td>ลงวันที่</td>
          <td><?= date_my2thai($budget->doc_date) ?></td>
        </tr>
        <tr>
          <td>รายละเอียด</td>
          <td><?= $budget->detail ?></td>
        </tr>
        <tr>
          <td>งบประมาณทั้งสิ้น</td>
          <td><?= number_format($sum_budget_detail->budget) ?></td>
        </tr>
        <!-- tr>
          <td>วันที่บันทึก</td>
          <td><?= datetime_my2thai($budget->insert_at, 'd mmm yyyy') ?></td>
        </+tr>
        <tr>
          <td>วันที่ปรับปรุงข้อมูล</td>
          <td><?= datetime_my2thai($budget->update_at, 'd mmm yyyy') ?></td>
        </tr -->
      </table>
    </div>

    <?php if(auth1_lv_chk('area')): ?>
    <div class='columns'>
      <div class="column">
        <a class="button is-warning" href="<?= site_url('budget/edit/').$budget->id ?>">แก้ไขข้อมูล</a>  
        <a class="button is-primary" href="<?= site_url('budget/info/').$budget->id.'/print' ?>" target=blank>พิมพ์รายงาน</a>  
      </div>
      <div class="column has-text-right">
        <a class="button is-danger button-confirm-delete" href="<?= site_url('budget/delete/').$budget->id ?>">ลบ</a>  
      </div>
    </div>
    <?php endif; ?>

  </div>
  </div>
</div>
<?php include("budget_detail_view.php"); ?>