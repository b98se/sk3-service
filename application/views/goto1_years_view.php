<link rel="stylesheet" href="<?php echo base_url("assets"); ?>/css/jquery.dataTables.min.css">

<div class="">
  <div class="column">
    <h5 class="title is-5"><?php echo $title ?></h5>  
  </div>

  <div class="columns">
    <?php 
    $html = '<div class="column"><div class="card has-background-success-light">
      <div class="card-content"><div class="content"> 
      <h1 class="title has-text-centered">
      <a href="'.site_url('goto1/index1/').'%s" >
        พ.ศ. %s (%s โรง) </a></h1>  
      </div></div></div></div>';
    foreach($goto1_years as $y){
      echo sprintf($html, $y->year1, ($y->year1 + 543), $y->cnt);

    }
    ?>
    <div class="column"></div>
  </div>


</div>

<script>

$(document).ready(function() {
    $('#table1').DataTable({"language": {"url": "<?= base_url("assets") ?>/js/datatableThai.json"}, "order" :[[1,"desc"]], "paging": false});
} );
</script>

<?php //print_r($goto_service);