<div class="p-4 has-background-info-light">
  <div class="columns">
    <div class="column">
      <h5 class="title is-5">ข้อมูลซ่อมบำรุง</h5>  
    </div>
    <div class="column has-text-right">
      <?php if(auth1_chk()): ?>
      <a class="button is-primary" href="<?= site_url('service/add/'.$school->smis) ?>">เพิ่มข้อมูล</a>  
      <?php endif; ?>
    </div>
  </div>

  <div class="table-container">
    <table id='table1services' class="table is-fullwidth">
      <!-- Your table content -->
      <thead>
        <tr>
          <th>ที่</th>
          <th>วันที่</th>
          <th>ระบบ</th>
          <th>ผู้ดำเนินการ</th>
        </tr>
      </thead>
      <tbody>
        <?php 
        $url = site_url('service/info');
        $trtd = "<tr><td>%s</td><td data-sort='%s'><a href='$url/%s'>%s</a></td><td>%s</td><td>%s</td>
                  </tr>";
        foreach($services as $i => $service){
          echo sprintf($trtd, ($i+1), $service->s_date, $service->id, date_my2thai($service->s_date, 'd mmm yyyy'),$stypes[$service->stype_id],$service->display_name);
        }
        ?>
      </tbody>
    </table>
  </div>
</div>
