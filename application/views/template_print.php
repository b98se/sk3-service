<?php 
$u = empty($this->session->userdata['logged_in']) ? false : $this->session->userdata['logged_in'];
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title> 
      <?= $title ?> ::
      <?= $this->config->item('sys_name_short') ?> ::
      <?= $this->config->item('office_name_short') ?>
    </title>
    <link rel="shortcut icon" href="<?= base_url("assets"); ?>/favicon.ico" />
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Kanit&family=Sarabun&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="<?= base_url("assets"); ?>/css/bulma.min.css">
    <link rel="stylesheet" href="<?= base_url("assets"); ?>/css/app.css">

    <script type="text/javascript" src="<?= base_url("assets"); ?>/js/jquery-3.6.0.min.js"></script>
    <script type="text/javascript" src="<?= base_url("assets"); ?>/js/jquery.dataTables.min.js"></script>

  </head> 
  <body>

  <?= $contents ?>
  
  </body>
</html>