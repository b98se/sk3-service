<div class="columns is-centered">
  <div class="column is-8-tablet is-6-desktop">
  <h5 class="title is-5"><?= $title ?></h5>

    <form action="<?php echo site_url('budget_detail/update') ?>" method='post' class="box">
      <input type="hidden" name="id" value="<?= $budget_detail->id ?>">
      <input type="hidden" name="budget_id" value="<?= $budget_detail->budget_id ?>">
      <?php include("budget_detail_form_view.php") ?>

<div class='columns'>
  <div class='column has-text-left'>
    <div class="field"><button class="button is-success">บันทึกข้อมูล</button></div>
  </div>
  <div class='column has-text-right'>
    <a class="button is-danger button-confirm-delete" href="<?= site_url('budget_detail/delete/').$budget_detail->budget_id.'/'.$budget_detail->id ?>">ลบ</a>
  </div>
</div>

    </form>
  </div>
</div>

<script>
jsObject = <?= json_encode($budget_detail) ?>

$.each( jsObject, function( key, value ) { 
  //console.log( key + ": " + value );
  $("[name="+key+"]").val(value);
});
</script>