<link rel="stylesheet" href="<?php echo base_url("assets"); ?>/css/jquery.dataTables.min.css">

<div class="">
  <div class="columns">
    <div class="column">
      <h5 class="title is-5"><?php echo $title ?></h5>  
    </div>
  </div>

  <div class="table-container">
    <table id='table1' class="table is-fullwidth">
      <!-- Your table content -->
      <thead>
        <tr>
          <th>ที่</th>
          <th>วันที่</th>
          <th>โรงเรียน</th>
          <th>ผู้ดำเนินการ</th>
          <th>การลงพื้นที</th>
        </tr>
      </thead>
      <tbody>
        <?php 
        $trtd = "<tr><td>%s</td> <td data-sort='%s'>%s</td> <td>%s</td> <td>%s</td> <td data-sort='%s'>%s</td> </tr>";

        foreach($goto1s as $i => $goto1){
          $service1 = $service1_date = '';
          $td_school = sprintf("<a href='%s'>%s</a>", (site_url('school/info/').$goto1->smis) ,($goto1->name .' - '.$goto1->amphur));

          $goto1date = date_my2thai($goto1->date1, 'd mmm yyyy');
          if($goto1->date2){
            // $ymd = DateTime::createFromFormat('Y-m-d H:i:s', $date_my);
            $date1 = DateTime::createFromFormat('Y-m-d', $goto1->date1);
            $date2 = DateTime::createFromFormat('Y-m-d', $goto1->date2);

            if ($date1->format('Y-m') === $date2->format('Y-m')) {

              $goto1date = $date1->format('d').' - '. date_my2thai($date2->format('Y-m-d'), 'd mmm yyyy');
            }else{

              $goto1date .= ' - '.date_my2thai($goto1->date2, 'd mmm yyyy');
            }

          }
          if (is_object($goto_service[$goto1->smis])) {
            // index.php/service/info/84 
            $service_ptn = '<a href="'.site_url('service/info/').'%s" target="blank" title="%s"> %s %s</a>';
            $x = $goto_service[$goto1->smis];

            $service_id = $x->service_id ? '' : '*';
            $service1 = sprintf($service_ptn, $x->id, $x->s_desc, date_my2thai($x->s_date, 'd mmm yyyy'), $service_id);
            $service1_date = $x->s_date;
          }


          echo sprintf($trtd, ($i+1), $goto1->date1, $goto1date, $td_school , $goto1->taker, $service1_date, $service1);
        }
        ?>
      </tbody>
    </table>
  </div>
</div>

<script>

$(document).ready(function() {
    $('#table1').DataTable({"language": {"url": "<?= base_url("assets") ?>/js/datatableThai.json"}, "order" :[[1,"desc"]], "paging": false});
} );
</script>

<?php //print_r($goto_service); 