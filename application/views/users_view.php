<link rel="stylesheet" href="<?php echo base_url("assets"); ?>/css/jquery.dataTables.min.css">

<div class="">
<h5 class="title is-5"><?= $title ?></h5>

<div class="table-container">
  <table id='table1' class="table is-fullwidth">
    <!-- Your table content -->
    <thead>
      <tr>
        <th>ที่</th>
        <th>username</th>
        <th>ชื่อ</th>
        <th>ประเภท</th>
        <th>การใช้</th>
      </tr>
    </thead>
    <tbody>
      <?php 
      $trtd = '<tr><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td></tr>';
      foreach($users as $user){
        echo sprintf($trtd, $user->id,$user->username,$user->display_name,$user->user_type, $user->lastseen);
      }
      ?>
    </tbody>
  </table>
</div>
</div>

<script>

$(document).ready(function() {
    $('#table1').DataTable({"language": {"url": "<?= base_url("assets") ?>/js/datatableThai.json"}});
} );
</script>