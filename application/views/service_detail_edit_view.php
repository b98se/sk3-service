<div class="columns is-centered">
  <div class="column is-8-tablet is-6-desktop">
  <h5 class="title is-5"><?= $title ?></h5>

    <form action="<?php echo site_url('service_detail/update') ?>" method='post' class="box" enctype="multipart/form-data">
      <input type="hidden" name="id" value="<?= $id ?>">
      <?php include("service_detail_form_view.php") ?>

      <div class='column has-text-right'>
        <img src='<?php echo base_url("uploads").'/service_detail_'.$id.'.jpg' ?>' />
      </div>

      <?php if(auth1_chk()): ?>
      <div class='columns'>
        <div class='column has-text-left'>
          <div class="field"><button class="button is-success">บันทึกข้อมูล</button></div>
        </div>
        <div class='column has-text-right'>
          <a class="button is-danger button-confirm-delete" href="<?= site_url('service/detail_delete/').$service->id.'/'.$id ?>">ลบ</a>
        </div>
      </div>
      <?php endif; ?>

    </form>
  </div>
</div>

<script>
jsObject = <?= json_encode($service_detail) ?>

$.each( jsObject, function( key, value ) {
  //console.log( key + ": " + value );
  $("[name="+key+"]").val(value);
});
</script>