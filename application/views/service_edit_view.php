<div class="columns is-centered">
  <div class="column is-8-tablet is-6-desktop">
  <h5 class="title is-5"><?= $title ?></h5>

    <form action="<?php echo site_url('service/update') ?>" method='post' class="box">
      <input type="hidden" name="id">
      <?php include("service_form_view.php") ?>

      <div class="field">
        <button class="button is-success">
          บันทึกข้อมูล
        </button>
      </div>
    </form>
  </div>
</div>



<script>
jsObject = <?= json_encode($service) ?>

$.each( jsObject, function( key, value ) {
  //console.log( key + ": " + value );
  if(key == 's_date'){
    value = date_my2thai(value);
  }
  $("[name="+key+"]").val(value);
});
</script>