<link rel="stylesheet" href="<?php echo base_url("assets"); ?>/css/jquery.dataTables.min.css">

<div class="">
  <div class="columns">
    <div class="column">
      <h5 class="title is-5">ข้อมูลซ่อมบำรุง</h5>  
    </div>
  </div>

  <div class="table-container">
    <table id='table1' class="table is-fullwidth">
      <!-- Your table content -->
      <thead>
        <tr>
          <th>ที่</th>
          <th>วันที่</th>
          <th>โรงเรียน</th>
          <th>อำเภอ</th>
          <th>ระบบ</th>
          <th>ผู้ดำเนินการ</th>
        </tr>
      </thead>
      <tbody>
        <?php 
        $trtd = "<tr><td>%s</td><td data-sort='%s'>%s</td><td>%s</td> <td>%s</td> <td>%s</td><td>%s</td></tr>";

        foreach($services as $i => $service){
          $td_service = sprintf("<a href='%s'>%s</a>", (site_url('service/info/').$service->id) ,date_my2thai($service->s_date, 'd mmm yyyy'));
          //$td_school = sprintf("<a href='%s'>%s</a>", (site_url('school/info/').$service->smis) ,($service->name));

          echo sprintf($trtd, ($i+1), $service->s_date, $td_service , $service->name, $service->amphur ,$stypes[$service->stype_id],$service->display_name);
        }
        ?>
      </tbody>
      <tfoot>
          <tr>
          <th>ที่</th>
          <th>วันที่</th>
          <th>โรงเรียน</th>
          <th>อำเภอ</th>
          <th>ระบบ</th>
          <th>ผู้ดำเนินการ</th>
          </tr>
      </tfoot>
    </table>
  </div>
</div>

<script>

$(document).ready(function() {
    $('#table1').DataTable({
      "language": {"url": "<?= base_url("assets") ?>/js/datatableThai.json"}, 
      "order" :[[1,"desc"]],
      "initComplete" : function () {
            this.api()
                .columns()
                .every(function () {
                    var column = this;
                    var select = $('<select><option value=""></option></select>')
                        .appendTo($(column.footer()).empty())
                        .on('change', function () {
                            var val = $.fn.dataTable.util.escapeRegex($(this).val());
 
                            column.search(val ? '^' + val + '$' : '', true, false).draw();
                        });
 
                    column
                        .data()
                        .unique()
                        .sort()
                        .each(function (d, j) {
                            select.append('<option value="' + d + '">' + d + '</option>');
                        });
                });
        },
    });
} );
</script>