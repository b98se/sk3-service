<div class="field">
  <label for="name" class="label">เรื่อง</label>
  <div class="control ">
    <input type="text" class="input" name='name'  required>
  </div>
</div>
<div class="field">
  <label for="doc_no" class="label">เลขที่หนังสือ</label>
  <div class="control ">
    <input type="text" class="input" name='doc_no'  required>
  </div>
</div>
<div class="field">
  <label for="doc_date" class="label">ลงวันที่</label>
  <div class="control ">
    <input type="text" class="input" name='doc_date'  required>
  </div>
</div>


<div class="field">
  <label for="" class="label">รายละเอียด</label>
  <div class="control ">
    <textarea class="textarea" name='detail'></textarea>
  </div>
</div>
