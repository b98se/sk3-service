<div class="columns is-centered">
  <div class="column is-8-tablet is-6-desktop">
  <h5 class="title is-5"><?= $title ?></h5>

    <form action="<?php echo site_url('budget/insert') ?>" method='post' class="box">
      <?php include("budget_form_view.php") ?>

      <div class="field">
        <button class="button is-success">
          บันทึกข้อมูล
        </button>
      </div>
    </form>
  </div>
</div>

<script>
$("input[name=doc_date]").val('<?= date_my2thai(date("Y-m-d")) ?>')
</script>