<div class="container">
  <div class="column has-text-centered ">
    <span class='has-text-weight-bold'>แบบเก็บข้อมูลการติดตาม พัฒนาเทคโลโลยีดิจิทัลและระบบสารสนเทศเพื่อการศึกษา</span>
    <br>
    โรงเรียน <?= $school->name ?>
    อำเภอ <?= $school->amphur ?>
  </div>
  <div class="column has-text-right">
    วันที่ <?= date_my2thai($service->s_date, 'd mmm yyyy') ?>
  </div>
  <div class="column has-text-left has-text-weight-bold">
    การตรวจสอบอุปกรณ์ที่ต้องการซ่อม
  </div>

  <?php 
  $ii = 1;
  foreach($items as $item_id => $item_name): 
  ?>

  <div class="column ">
    <span title='<?= $item_id ?>'><?= $item_name ?></span>
    <table id='table1' class="table is-bordered is-fullwidth">
      <!-- Your table content -->
      <thead>
        <tr>
          <th width='10%' >ที่</th>
          <th width='30%'>อาการ</th>
          <th width='15%'>ยี่ห้อ</th>
          <th width='15%'>รุ่น</th>
          <th width='30%'>แนวทางแก้ไข</th>
        </tr>
      </thead>
      <tbody>
        <?php 
        // service_id	item_id	symptom	item_brand	item_model	solve	update_at	insert_at
        $trtd = "<tr><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td></tr>";
        foreach($svds as $i => $svd){
          if($item_id == $svd->item_id){
            echo sprintf($trtd, ($ii++), ($svd->item_etc.$svd->symptom), $svd->item_brand, $svd->item_model, $svd->solve);
          }
        }
        echo str_replace('%s', '', $trtd);
        ?>
      </tbody>
    </table>
  </div>


  <?php endforeach; ?>

  <div class="column ">
    <?= $service->s_desc ?>
  </div>

</div>
