<div class="columns is-centered">
  <div class="column is-10-tablet ">
    
  <div class="box">
    <h5 class="title is-5"><?= $title ?></h5>
    
    <div class="table-container">
      <table id='table1' class="table is-fullwidth is-striped table-detail">
        <!-- Your table content 
        id user_id smis stype_id s_date s_desc update_at insert_at         -->
        <tr>
          <td>รหัส smis</td>
          <!-- http://v7.sk3.go.th/ms/index.php/school/info/90030023 -->
          <td><a href='<?= site_url('school/info/').$service->smis ?>' ><?= $service->smis ?></a></td>
        </tr>
        <tr>
          <td>โรงเรียน</td>
          <td><?= $service->name ?></td>
        </tr>
        <tr>
          <td>ระบบ</td>
          <td><?= $stypes[$service->stype_id] ?></td>
        </tr>
        <tr>
          <td>วันที่ซ่อมบำรุง</td>
          <td><?= date_my2thai($service->s_date) ?></td>
        </tr>
        <tr>
          <td>ข้อเสนอแนะเพิ่มเติม</td>
          <td><?= $service->s_desc ?></td>
        </tr>
        <tr>
          <td>ผู้บันทึก</td>
          <td>
            <?= $service->display_name ?> 
            (<?= $service->update_at ? datetime_my2thai($service->update_at) : datetime_my2thai($service->insert_at) ?>)
          </td>
        </tr>
        <!--tr>
          <td>วันที่บันทึก</td>
          <td><?= datetime_my2thai($service->insert_at) ?></td>
        </!--tr>
        <tr>
          <td>วันที่ปรับปรุงข้อมูล</td>
          <td><?= datetime_my2thai($service->update_at) ?></td>
        </tr -->
      </table>
    </div>

    <?php if(auth1_chk()): ?>
    <div class='columns'>
      <div class="column">
        <a class="button is-warning" href="<?= site_url('service/edit/').$service->id ?>">แก้ไขข้อมูล</a>  
        <a class="button is-primary" href="<?= site_url('service/info/').$service->id.'/print' ?>" target=blank>พิมพ์รายงาน</a>  
      </div>
      <div class="column has-text-right">
        <a class="button is-danger button-confirm-delete" href="<?= site_url('service/delete/').$service->id ?>">ลบ</a>  
      </div>
    </div>
    <?php endif; ?>

  </div>
  </div>
</div>
<?php include("service_detail_view.php"); ?>