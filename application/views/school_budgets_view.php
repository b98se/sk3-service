<div class="p-4 has-background-warning-light">
  <div class="columns">
    <div class="column">
      <h5 class="title is-5">ข้อมูลการจัดสรรงบประมาณ</h5>  
    </div>
    <div class="column has-text-right">
      <?php if(auth1_lv_chk('area')): ?>
      <a class="button is-primary" href="<?= site_url('budget/add/'.$school->smis) ?>">เพิ่มการจัดสรรงบ</a>  
      <?php endif; ?>
    </div>
  </div>

  <div class="table-container">
    <table id='table1school_budgets' class="table is-fullwidth">
      <!-- Your table content -->
      <thead>
        <tr>
          <th>ที่</th>
          <th>เลขที่</th>
          <th>ลงวันที่</th>
          <th>เรื่อง</th>
          <th>งบประมาณ</th>
          <th>หมายเหตุ</th>
        </tr>
      </thead>
      <tbody>
        <?php 
        $url = site_url('budget/info');
        $trtd = "<tr><td>%s</td> <td><a href='$url/%s'>%s</a></td> <td>%s</td> 
                  <td>%s</td>  <td>%s</td> <td>%s</td>
                  </tr>";
        foreach($school_budgets as $i => $school_budget){
          $doc_date1 = date_my2thai($school_budget->doc_date, 'd mmm yyyy');
          echo sprintf($trtd, ($i+1), $school_budget->id, $school_budget->doc_no, 
            $doc_date1, $school_budget->name, $school_budget->budget, $school_budget->etc );
        }
        ?>
      </tbody>
    </table>
  </div>
</div>