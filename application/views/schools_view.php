<link rel="stylesheet" href="<?php echo base_url("assets"); ?>/css/jquery.dataTables.min.css">

<div class="">
<h5 class="title is-5"><?= $title ?></h5>

<div class="table-container">
  <table id='table1' class="table is-fullwidth">
    <!-- Your table content -->
    <thead>
      <tr>
        <th>ที่</th>
        <th>smis</th>
        <th>โรงเรียน</th>
        <th>อำเภอ</th>
        <th>ขนาด</th>
      </tr>
    </thead>
    <tbody>
      <?php 
      // print_r($sch_sizes);
      $url = site_url('school/info');
      $trtd = "<tr><td>%s</td><td><a href='$url/%s'>%s</a></td><td>%s</td><td>%s</td><td>%s</td></tr>";
      foreach($schools as $i => $school){
        echo sprintf($trtd, ($i+1), $school->smis, $school->smis,$school->name,$school->amphur,$sch_sizes[$school->sch_size]);
      }
      ?>
    </tbody>
  </table>
</div>
</div>

<script>

$(document).ready(function() {
    $('#table1').DataTable({"language": {"url": "<?= base_url("assets") ?>/js/datatableThai.json"}});
} );
</script>