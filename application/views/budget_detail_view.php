<link rel="stylesheet" href="<?php echo base_url("assets"); ?>/css/jquery.dataTables.min.css">

<div class="">

  <div class="columns">
    <div class="column">
      <h5 class="title is-5">รายการจัดสรรงบประมาณ</h5>  
    </div>

    <?php if(auth1_lv_chk('area')): ?>
    <div class="column has-text-right">
      <a class="button is-primary" href="<?= site_url('budget_detail/add/').$budget->id ?>">เพิ่มรายการ</a>  
    </div>
    <?php endif; ?>
  </div>


<div class="table-container">
  <table id='table1' class="table is-fullwidth">
    <!-- Your table content -->
    <thead>
      <tr>
        <th>ที่</th>
        <th>โรงเรียน</th>
        <th>อำเภอ</th>
        <th>งบประมาณ</th>
        <th>หมายเหตุ</th>
      </tr>
    </thead>
    <tbody>
      <?php 
      // service_id	item_id	symptom	item_brand	item_model	solve	update_at	insert_at
      $url = site_url('budget_detail/edit');
      $trtd = "<tr><td>%s</td><td><a href='$url/%s'>%s</a></td> <td>%s</td> <td>%s</td> <td>%s</td></tr>";
      
      foreach($budget_details as $i => $budget_detail){
        echo sprintf($trtd, ($i+1), $budget_detail->id, $budget_detail->name, $budget_detail->amphur, number_format($budget_detail->budget), $budget_detail->etc);
      }
      ?>
    </tbody>
  </table>
</div>
</div>

<script>

$(document).ready(function() {
    $('#table1').DataTable({"language": {"url": "<?= base_url("assets") ?>/js/datatableThai.json"}});
} );
</script>