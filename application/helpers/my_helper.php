<?php
function list1s2array($obj){
    foreach($obj as $o){
        $ar[$o->key1] = $o->value1;
    }
    return $ar;
}

function date_my2thai($date_my, $fmt = 'd/m/y'){
    $ymd = DateTime::createFromFormat('Y-m-d', $date_my);
    if($ymd == false) return '';

    $month_long = config_item('month_long');

    $y2000 = $ymd->format('Y'); 
    $y = $y2000 + 543;
    $m = str_pad($ymd->format('m'),2,"0",STR_PAD_LEFT);
    $d = str_pad($ymd->format('d'),2,"0",STR_PAD_LEFT);

    if($fmt=='d/m/y'){
        $rtn = $d.'/'.$m.'/'.$y;
    }else if($fmt=='d mmm yyyy'){
        $rtn = $d.' '.$month_long[$m].' '.$y;
    }
    
    return $rtn;
}
function datetime_my2thai($date_my, $fmt = 'd/m/y'){
    $ymd = DateTime::createFromFormat('Y-m-d H:i:s', $date_my);
    if($ymd == false) return '';

    $month_long = config_item('month_long');

    $y2000 = $ymd->format('Y');  
    $y = $y2000 + 543;
    $m = str_pad($ymd->format('m'),2,"0",STR_PAD_LEFT);
    $d = str_pad($ymd->format('d'),2,"0",STR_PAD_LEFT);
    $t = $ymd->format('H:i');

    if($fmt=='d/m/y'){
        $rtn = $d.'/'.$m.'/'.$y.' '.$t;
    }else if($fmt=='d mmm yyyy'){
        $rtn = $d.' '.$month_long[$m].' '.$y.' เวลา '.$t;
    }
    
    return $rtn;
}

function date_thai2my($date){

    list($d,$m,$Y) = explode('/',$date);
	$d = str_pad($d,2,"0",STR_PAD_LEFT);
	$m = str_pad($m,2,"0",STR_PAD_LEFT);
    $Y = $Y-543;
    return $Y.'-'.$m.'-'.$d;
    // return $Y.$m.$d;
}

function get_percentage($total, $number)
{
    if ( $total > 0 ) {
        return round($number / ($total / 100),0);
    } else {
        return 0;
    }
}
function set_grade($score){

    if( $score >= 80 ){
        return "success";
    }else if( $score >= 60 && $score < 80 ){
        return "info";
    }else if( $score >= 50 && $score < 60 ){
        return "primary";
    }else if( $score >= 10 && $score < 50 ){
        return "warning";
    }else{
        return "danger";
    }
}

function svd_item_name($items, $item_id, $item_etc){
    if($item_id == 'etc'){
        $item2 = $items[$item_id] . ' ('. $item_etc .')';
    }else{
        $item2 = $items[$item_id];
    }
    return $item2;
}

function auth1_chk (){
    $ci =& get_instance();

    return $ci->session->logged_in==true ? true : false;
}
function auth1 (){

    if(!auth1_chk()){
        $ci =& get_instance();
        $ci->session->set_flashdata('alert','danger | กรุณา Login เข้าสู่ระบบ !!! ');
        redirect(site_url('/'));
    }
}

function auth1_lv_chk ($level = ''){
    $ci =& get_instance();
    $u = $ci->session->logged_in;
    
    if($u){
        if($u['user_type'] == $level){
            return true;
        }else{
            return false;
        }
        
    }else{
        return false;
    }
}

function auth1_lv ($level = ''){
    $ci =& get_instance();

    if(!auth1_lv_chk($level)){
        $ci->session->set_flashdata('alert','warning | บัญชีผู้ใช้ของท่าน ไม่มีสิทธิ์ใช้งานส่วนนี้ !!! ');
        redirect(site_url('/'));
    }
}

function db_result_redirect($result, $redirect){
    $ci =& get_instance();

    if($result == true){
      $ci->session->set_flashdata('alert','success | บันทึกข้อมูลเรียบร้อยแล้ว !!! ');
      redirect($redirect);
    }else{
      $ci->session->set_flashdata('alert','danger | เกิดข้อผิดพลาดในการบันทึกข้อมูล !!! ');
      redirect($_SERVER['HTTP_REFERER']);
    }
}

function echo1 ($txt, $format = ''){
    return $txt==true ? sprintf($format, $txt) : '';
}

function year_season ($date1 = false){
    if($date1){
        $date2 = DateTime::createFromFormat('Y-m-d', $date1);
    }else{
        $date2 =  new DateTime();
    }
    if($date2->format('n') >= 10 ){
        $y = $date2->format('Y') - 1;
    }else{
        $y = $date2->format('Y');
    }
    return $y;
}

function findObjectById($id, $array, $find, $return){
    foreach($array as $a){
        if($a->$id == $find){
            return $a->$return;
            break;
        }
    }
    return 0;
}

