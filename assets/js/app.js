function date_my2thai(date) {
  var ymd = date.split("-");
  year = parseInt(ymd[0]) + 543;
  month = ymd[1].padStart(2, '0');
  day = ymd[2].padStart(2, '0');

  return [day, month, year].join('/');
}